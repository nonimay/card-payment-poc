package uk.nhs.nhsbsa.servicea.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;

@Controller
public class ServiceAController {

    @GetMapping("/")
    public String get() {
        return "pay";
    }

    @GetMapping("/complete")
    public String getComplete() {
        return "complete";
    }

    @PostMapping("make-payment")
    public String post() {
        RestTemplate template = new RestTemplate();
        ResponseObject response = template.postForObject("http://localhost:8081/create-payment", null, ResponseObject.class);

        return "redirect:" + response.getUrl();
    }
}

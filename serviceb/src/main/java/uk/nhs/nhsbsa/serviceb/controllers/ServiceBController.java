package uk.nhs.nhsbsa.serviceb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;

@Controller
public class ServiceBController {

    Logger logger = Logger.getLogger(this.getClass().getName());

    @PostMapping("/create-payment")
    @ResponseBody
    public PostResponse createPayment(HttpServletResponse response) {
        logger.info("Creating Payment in Service B");

        //get session cookie to save
        response.addCookie(new Cookie("BSA_COOKIE", "ABC123"));

        //save new create payment to DB

        //posting to GovPay
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://localhost:8080/create-payment";
        PostResponse postResponse = restTemplate.postForObject(url, null, PostResponse.class);

        //update DB with self url and status

        //return the next url to PPC
        return postResponse;

    }

    @GetMapping("/complete")
    public String getComplete(HttpServletRequest request) {
        //find self url from session cookie
        logger.info(request.getCookies()[0].getValue());
        //do status update
        //redirect to complete page
        return "redirect:http://localhost:8082/complete";

    }


    public void doStatusUpdate() {
        //get status from self url at govpay
        //update db with status
        //post status to PPC/TLRS
    }

}
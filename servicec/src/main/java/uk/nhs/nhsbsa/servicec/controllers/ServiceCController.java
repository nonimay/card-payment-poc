package uk.nhs.nhsbsa.servicec.controllers;

import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Logger;


@Controller
public class ServiceCController {

    Logger logger = Logger.getLogger(this.getClass().getName());

    @GetMapping("/next-url")
    public String getNextUrl(HttpServletRequest request) {
        logger.info(request.getCookies()[0].getName());
        return "next";
    }

    @PostMapping("/create-payment")
    @ResponseBody
    public Response createPayment() {
        logger.info("Creating Payment in Service C");
        return new Response("http://localhost:8080/next-url");
    }

    @PostMapping("submit-payment")
    public String submitPayment() {
        //take a payment
        //call bsa card payment service
        return "redirect:http://localhost:8081/complete";
    }


}

package uk.nhs.nhsbsa.servicec.controllers;

public class Response {

    private String url;

    public Response(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
